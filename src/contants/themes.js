export const themes = {
  light: {
    buttons: {
      tag: {
        color: '#FFFFFF',
      },
    },
    card: {
      borderColor: '#F1F1F1',
    },
    chip: {
      backgroundColor: '#C4C4C4',
      textColor: '#fff',
      borderColor: '#C4C4C4',
      activeBackgroundColor: '#000',
      activeTextColor: '#fff',
      activeBorderColor: '#000000',
    },
    colors: {
      white: '#ffffff',
      dark: '#090C1D',
      secondary: '#fff',
      tertiary: '#f3f3f3',
    },
    containerBackground: '#ffffff',
    drawer: {
      backgroundColor: '#ffffff',
      borderColor: '#F1F1F1',
    },
    header: {
      borderColor: '#d5d5d5',
    },
    inactiveButton: '#9096B4',
    input: {
      borderColor: '#000000',
      switch: {
        darkMode: '#9096B4',
        lightMode: '#0F4DFF',
        backgroundColor: '#0F4DFF',
      },
    },
    primaryButton: '#0F4DFF',
    sectionTitle: {
      backgroundColor: '#d5d5d5',
    },
    statusBar: {
      backgroundColor: '#ffffff',
      barStyle: 'dark-content',
    },
    textColor: '#000000',
    textContrastColor: '#000000',
    themeName: 'light',
    activityIndicator: '#000',
  },
  dark: {
    buttons: {
      tag: {
        color: '#9096B4',
      },
    },
    card: {
      borderColor: '#151A2E',
    },
    chip: {
      backgroundColor: '#090C1D',
      textColor: '#fff',
      borderColor: '#ffffff',
      activeBackgroundColor: '#fff',
      activeTextColor: '#090C1D',
      activeBorderColor: '#ffffff',
    },
    containerBackground: '#090C1D',
    colors: {
      white: '#ffffff',
      dark: '#090C1D',
      secondary: '#151A2E',
      tertiary: '#151a2e',
    },
    drawer: {
      backgroundColor: '#090C1D',
      borderColor: '#151A2E',
    },
    header: {
      borderColor: '#151a2e',
    },
    inactiveButton: '#9096B4',
    input: {
      borderColor: '#9096B4',
      switch: {
        thumbColor: '#ffffff',
        darkMode: '#9096B4',
        lightMode: '#0F4DFF',
        backgroundColor: '#9096B4',
      },
    },
    primaryButton: '#0F4DFF',
    sectionTitle: {
      backgroundColor: '#2f3347',
    },
    statusBar: {
      backgroundColor: '#090C1D',
      barStyle: 'light-content',
    },
    textColor: '#9096B4',
    textContrastColor: '#ffffff',
    themeName: 'dark',
    activityIndicator: '#9096B4',
  },
};
