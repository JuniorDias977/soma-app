export const Screens = {
  APP: {
    navigator: 'App',
    REGISTER_SCREEN: 'RegisterScreen',
    USER_PROFILE_SCREEN: 'UserProfileScreen',
    DELETE_ACCOUNT_SCREEN: 'DeleteAccountScreen',
    HOME_SCREEN: 'Home',
    EDIT_PROFILE_SCREEN: 'EditProfileScreen',
  },
  NEWS: {
    navigator: 'News',
    INTERESTS_SCREEN: 'InterestsScreen',
    NEWS_DETAIL: 'NewsDetailScreen',
    SEARCH_NEWS: 'SearchNewsScreen',
  },
  EVENTS: {
    navigator: 'Events',
    EVENTS_SCREEN: 'EventsScreen',
    EVENT_DETAILS_SCREEN: 'EventDetailsScreen',
    EVENT_MAP_DETAILS_SCREEN: 'EventMapDetailsScreen',
    EVENT_CONFIRMED_PEOPLE_SCREEN: 'EventConfirmedPeopleScreen',
    EVENT_GALLERY_SCREEN: 'EventGalleryScreen',
  },
  DISCOUNTS_CLUB: {
    navigator: 'DiscountsClub',
    DISCOUNTS_CLUB_SCREEN: 'DiscountsClubScreen',
    DISCOUNT_MAP_DETAILS_SCREEN: 'DiscountMapDetailsClubScreen',
    DISCOUNT_DETAILS_SCREEN: 'DiscountDetailsScreen',
    DISCOUNT_CATEGORIES_SCREEN: 'DiscountCategoriesScreen',
    DISCOUNT_CREATE_DISCOUNT_SCREEN: 'DiscountCreateScreen',
  },
  AFFILIATED: {
    navigator: 'Affiliated',
    AFFILIATED_SCREEN: 'AffiliatedScreen',
    AFFILIATED_DETAILS: 'AffiliatedDetailsScreen',
  },
  FORUM: {
    navigator: 'Forum',
    FORUM_SCREEN: 'ForumScreen',
    FORUM_CREATE_POST_SCREEN: 'ForumCreatePostScreen',
    FORUM_POST_DETAILS_SCREEN: 'ForumPostDetailsScreen',
  },
  NOTIFICATIONS: {
    navigator: 'Notifications',
    NOTIFICATIONS_SCREEN: 'NotificationsScreen',
  },
};
