import React from 'react';
import styled from 'styled-components';
import {ScrollView, View} from 'react-native';
import Loading from '../Loading';
import {SafeAreaView} from 'react-native-safe-area-context';
import HeaderOptions from './HeaderOptions';
import Text from '../Typography/Text';

const CustomSafeAreaScrollView = styled(ScrollView)`
  background-color: ${props => props.theme.containerBackground};
  flex: 1;
`;

export default ({
  children,
  loading,
  canGoBack = false,
  style,
  RightOptionsComp,
  canGoBackStyle,
  noPadding = false,
  noHeaderPadding = false,
  title,
  ...rest
}) => (
  <CustomSafeAreaScrollView
    scrollIndicatorInsets={{right: Number.MIN_VALUE}}
    {...rest}
    style={[
      style,
      {
        paddingHorizontal: loading || noPadding ? 0 : 24,
      },
    ]}>
    {loading ? (
      <Loading />
    ) : (
      <>
        {(canGoBack || RightOptionsComp) && (
          <HeaderOptions
            backButtonStyle={canGoBackStyle}
            noHeaderPadding={noHeaderPadding}
            canGoBack={canGoBack}
            RightOptionsComp={RightOptionsComp}
          />
        )}

        {title && (
          <Text
            style={{
              textAlign: 'center',
            }}
            size={18}
            fontStyle="bold">
            Excluir conta
          </Text>
        )}

        <SafeAreaView forceInset={{bottom: 'never'}}>{children}</SafeAreaView>
      </>
    )}
  </CustomSafeAreaScrollView>
);
