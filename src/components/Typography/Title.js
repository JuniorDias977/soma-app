import styled from 'styled-components';
import Text from './Text';

export default styled(Text)`
  font-weight: 500;
  font-size: 14px;
  line-height: 22px;
`;
