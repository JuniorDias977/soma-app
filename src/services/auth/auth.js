import {createUserInfoListenerService} from '../users';
import {store} from '../../redux/configureStore';
import {AuthActions} from '../../redux/actions';
import {fireAuth} from '../../config/firebase';
import {catchError} from '../../helpers/errors';

export const getAuthState = () => {
  fireAuth.onAuthStateChanged(async loggedUser => {
    if (loggedUser) {
      await syncUserInfo(loggedUser.uid);
    } else {
      signOut();
    }
  });
};

export const syncUserInfo = userId => {
  createUserInfoListenerService(userId, user => {
    store.dispatch(AuthActions.userLoggedIn(user));
  });
};

export const signOut = async () => {
  try {
    await fireAuth.signOut();
    store.dispatch(AuthActions.userLoggedOut());
  } catch (e) {
    catchError(e);
    store.dispatch(AuthActions.userLoggedOut());
  }
};
