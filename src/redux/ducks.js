import auth from './auth/duck';
import {reducer as appConfig} from './appConfig/duck';
import states from './states/duck';

export default {
  auth,
  appConfig,
  states,
};
