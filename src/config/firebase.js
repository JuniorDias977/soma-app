import nativeFirestore from '@react-native-firebase/firestore';
import nativeAuth from '@react-native-firebase/auth';
import nativeStorage from '@react-native-firebase/storage';
import nativeMessaging from '@react-native-firebase/messaging';

export const fireAuth = nativeAuth();
export const firestore = nativeFirestore();
export const storage = nativeStorage();
export const messaging = nativeMessaging();

const apiDevKey = "AIzaSyCMVUbaPQNCF1FzDHimNKGXudchDw61CuY"
const apiProductionKey = "AIzaSyChxNdxSPluskBuBgvgkgtrvtdnRu6VQTM"

export const firebaseConfig = {
  apiKey: apiProductionKey,
};
